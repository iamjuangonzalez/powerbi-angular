import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PowerBiComponent } from './components/power-bi/power-bi.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'reporte',
    pathMatch: 'full'
  },
  {
    path: 'reporte',
    component: PowerBiComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
