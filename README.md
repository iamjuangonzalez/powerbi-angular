# Configuración de Angular y Power Bi

Para el despligue de nuestra aplicación en Angular local vamos a pararnos en nuestra carpeta raiz powerbi-imagineApps>
Posteriormente abriremos nuestra consola de nuestro sistema operativo y ejecutaremos los siguientes comandos

## Installation Angular

Install powerbi-imagineApps> with npm

```bash
  npm install powerbi-imagineApps>
  cd my-project
  npm start
  Run `ng serve` for a dev server.
  Navigate to `http://localhost:4200/`.
  The application will automatically reload if you change any of the source files.
```

## Installation Power Bi

Nos dirigimos a la ruta https://app.powerbi.com/ donde vamos a iniciar sesión con nuestra cuenta de empresa o educativa (El administrador debe contar con el paquete de Power Bi en el plan educativo).

Nos dirigimos a la mesa de trabajo que actualmente estas trabajando, buscas el proyecto que guardaste. Abres el archivo, posteriormente generas un link publico para poder conectarnos externamente. Luego nos genera un codigo html para insertar en nuestro codigo.

```bash
  <iframe title="prueba" width="600" height="373.5"
  src="https://app.powerbi.com/view?r=eyJrIjoiZmRlNjNhMGUtY2I0MC00OTU2LTk3ZDctZGVjYmJh
  ZDVkNjQ2IiwidCI6IjE3YTkyM2I0LTZmM2UtNDc2Ny0
  5OGI2LWRmZjE1NzJiNmZiNyJ9" frameborder="0" allowFullScreen="true"></iframe>

```

De esta manera se incluye nuestro grafico de manera dinamica.

## License

[MIT](https://choosealicense.com/licenses/mit/)

## Optimizations

What optimizations did you make in your code? E.g. refactors, performance improvements, accessibility

## Support

For support, email juanjosegonzalezdulcey@gmail.com or join our Slack channel.

## Authors

- [@iamjuangonzalez](https://www.github.com/iamjuangonzalez)
